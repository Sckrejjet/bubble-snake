#ifndef MAIN_WIDGET_H
#define MAIN_WIDGET_H

#include <QWidget>
#include <QSound>

//class QSound;

#include "bubble.h"

class QTimer;

class MainWidget : public QWidget
{
    Q_OBJECT

public:
    MainWidget(QWidget *parent = 0);
    ~MainWidget();

    void addItem(Bubble* _bub);

    void chpok();

private slots:
    //
    void slotTimerStep();

    //
    void keyPressEvent(QKeyEvent* e);
    void keyReleaseEvent(QKeyEvent* e);

private:
    //
    Qt::Key currentKey_;

    //
    QList<Bubble*> listItems_;

    //
    Bubble* bubble_;
    Bubble* bubble2_;
    Bubble* bubble3_;
    Bubble* bubble4_;

    //
    double modelTime_;

    //
    QLabel* lab_;

    //
    QSound sound;

    //
    QTimer* timerStep_;
};

#endif // MAIN_WIDGET_H
