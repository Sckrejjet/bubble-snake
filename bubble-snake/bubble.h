#ifndef BUBBLE_H
#define BUBBLE_H

#include <QLabel>
class MainWidget;

class Bubble : public QLabel
{
public:
    Bubble(MainWidget* parent);

    enum class Type
    {
        HEAD,
        TALE
    };

    void step(double t, double dt, Qt::Key dir = Qt::Key_unknown);

//    void move(int x, int y);
//    void move(const QPoint &);

    double speed() const;
    void setSpeed(double speed);

    double angle() const;
    void setAngle(double angle);

    void setTarget(Bubble *target);

    void move(double x, double y);

    void resize(int w, int h);

public slots:

    void forcedKeyEvent(Qt::Key _key);

private:
    MainWidget* viewPort_;

    double x_;
    double y_;
    double angle_;
    double speed_;
    Bubble* target_;

    void draw_();

    void checkBounds_();
};

#endif // BUBBLE_H
