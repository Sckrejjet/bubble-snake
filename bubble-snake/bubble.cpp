#include "bubble.h"

#include <QPainter>
#include <qmath.h>

#include "main-widget.h"

Bubble::Bubble(MainWidget *parent)
    : QLabel(parent)
    , viewPort_(parent)
    , x_(0.0)
    , y_(0.0)
    , angle_(0.0)
    , speed_(0.0)
    , target_(Q_NULLPTR)
{
    this->resize(10, 10);
}

void Bubble::step(double t, double dt, Qt::Key dir)
{
    Q_UNUSED(t);

    switch (dir)
    {
    //
    case Qt::Key_Left:
        angle_ -= qDegreesToRadians(5.0);
        break;

    //
    case Qt::Key_Right:
        angle_ += qDegreesToRadians(5.0);
        break;

//    //
//    case Qt::Key_Up:
//        this->move(this->x(), this->y() - 5);
//        break;

//    //
//    case Qt::Key_Down:
//        this->move(this->x(), this->y() + 5);
//        break;

    default:
        if (target_ != Q_NULLPTR)
        {
            double catX = this->x() - target_->x();
            double catY = this->y() - target_->y();
            // old
//            double catX = target_->x() - this->x();
//            double catY = -target_->y() + this->y();
            double hyp = sqrt(catX*catX + catY*catY);
            if (hyp < 10)
                return;
//            angle_ = atan2(catY, catX);
            angle_ = atan2(catY, catX);
            angle_ += M_PI;
//            angle_ += M_PI_2;
        }
        break;
    }

    checkBounds_();

    double ds = speed_*dt;
    double dx = cos(angle_)*ds;
    double dy = sin(angle_)*ds;
    // old
//    double dx = sin(angle_)*ds;
//    double dy = cos(angle_)*ds;
    x_ += dx;
    y_ += dy;
    this->move(x_, y_);
}

double Bubble::speed() const
{
    return speed_;
}

void Bubble::setSpeed(double speed)
{
    speed_ = speed;
}

double Bubble::angle() const
{
    return angle_;
}

void Bubble::setAngle(double angle)
{
    angle_ = angle;
}

void Bubble::setTarget(Bubble *target)
{
    target_ = target;
}

void Bubble::move(double x, double y)
{
    x_ = x;
    y_ = y;
    this->QLabel::move(static_cast<int>(x), static_cast<int>(y));
}

void Bubble::resize(int w, int h)
{
    this->QLabel::resize(w, h);
    draw_();
}

void Bubble::forcedKeyEvent(Qt::Key _key)
{
    if (target_ == Q_NULLPTR)
    {
        if (_key == Qt::Key_Space)
        {
            Bubble* bub = new Bubble(viewPort_);
            bub->resize(5, 5);
            bub->move(x_, y_);
            bub->setSpeed(400.0);
            bub->setAngle(angle_);
            viewPort_->addItem(bub);
        }
    }
}


void Bubble::draw_()
{
    QImage img(this->size(), QImage::Format_ARGB32_Premultiplied);
    img.fill(Qt::transparent);
    QPainter p(&img);
    p.setPen(Qt::red);
    p.setBrush(Qt::red);
    p.drawEllipse(0, 0, this->width() - 1, this->height() - 1);
    p.end();
    this->setPixmap(QPixmap::fromImage(img));
}

void Bubble::checkBounds_()
{
    if (x_ >= viewPort_->width() || x_ <= 0.0)
    {
//        angle_ += (M_PI - angle_)*2;
        angle_ = M_PI - angle_;
        viewPort_->chpok();
    }

    if (y_ <= 0 || y_ >= viewPort_->height())
    {
        angle_ = - angle_;
        viewPort_->chpok();
    }
}

//void Bubble::move(int x, int y)
//{
//    x_ = static_cast<double>(x);
//    y_ = static_cast<double>(y);
//}

//void Bubble::move(const QPoint &p)
//{
//    x_ = static_cast<double>(p.x());
//    y_ = static_cast<double>(p.y());
//}
