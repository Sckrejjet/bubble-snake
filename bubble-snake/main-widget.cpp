#include "main-widget.h"

#include <QTimer>
#include <QKeyEvent>
//#include <QSound>

#include <QDebug>
#include <qmath.h>


constexpr int STEP = 20;

const QVector<Qt::Key> KEY_LIST{Qt::Key_Left, Qt::Key_Right, Qt::Key_Up,
                                Qt::Key_Down, Qt::Key_Space};

MainWidget::MainWidget(QWidget *parent)
    : QWidget(parent)
    , currentKey_(Qt::Key_unknown)
    , bubble_(new Bubble(this))
    , bubble2_(new Bubble(this))
    , bubble3_(new Bubble(this))
    , bubble4_(new Bubble(this))
    , modelTime_(0.0)
    , lab_(new QLabel(this))
    , sound("../CHpok.wav", this)
    , timerStep_(new QTimer(this))
{    
    this->resize(500, 400);

    bubble_->setSpeed(50.0);
    bubble_->move(300, 300);

    bubble2_->setSpeed(50.0);
    bubble2_->move(250, 250);
    bubble2_->setTarget(bubble_);

    bubble3_->setSpeed(50.0);
    bubble3_->move(250, 250);
    bubble3_->setTarget(bubble2_);

    bubble4_->setSpeed(50.0);
    bubble4_->move(250, 250);
    bubble4_->setTarget(bubble3_);

    timerStep_->setInterval(STEP);

    connect(timerStep_, SIGNAL(timeout()),
            this, SLOT(slotTimerStep()));
    timerStep_->start();
}

MainWidget::~MainWidget()
{

}

void MainWidget::addItem(Bubble *_bub)
{
    listItems_.append(_bub);
    _bub->show();
}

void MainWidget::chpok()
{
    sound.play();
}

void MainWidget::slotTimerStep()
{
    double dt = STEP/1000.0;
    modelTime_ += dt;
    bubble_->step(modelTime_, dt, currentKey_);
    bubble2_->step(modelTime_, dt);
    bubble3_->step(modelTime_, dt);
    bubble4_->step(modelTime_, dt);

    for (Bubble* bub : listItems_)
    {
        bub->step(modelTime_, dt);
    }

    lab_->setText(QString::number(qRadiansToDegrees(bubble_->angle())));
    lab_->adjustSize();
//    qDebug() << "step";
}

void MainWidget::keyPressEvent(QKeyEvent *e)
{
    if (KEY_LIST.contains(Qt::Key(e->key())))
    {
        currentKey_ = Qt::Key(e->key());
        bubble_->forcedKeyEvent(Qt::Key(e->key()));
    }
}

void MainWidget::keyReleaseEvent(QKeyEvent *e)
{
//    if (KEY_LIST.contains(Qt::Key(e->key())))
    if (currentKey_ == Qt::Key(e->key()))
    {
        currentKey_ = Qt::Key_unknown;
    }
}
